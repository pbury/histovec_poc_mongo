import os
import subprocess
import glob
from pymongo import MongoClient
import re

client = MongoClient('localhost', 27017)
db = client.local
collection = db.histovec_log

mongo_import_command = "mongoimport -c=histovec -d=local  --type=csv --headerline --file=%s 2>&1"
path = os.getcwd() + '/../input_data/'

files = sorted([f for f in glob.glob(path + "*_input.csv", recursive=False)])

for chunck in sorted(files):
    still_processed = collection.find_one({'filename': os.path.basename(chunck)})
    with open(chunck) as my_file:
        nb_lines_in_file = sum(1 for _ in my_file)
    if still_processed is None or still_processed['nb_lines_in_file'] != still_processed['nb_inserted']:
        final_command = mongo_import_command % chunck
        try:
            stdout = subprocess.check_output(final_command, shell=True)
            nb_inserted = int(re.findall(b'/*imported (\d*) documents', stdout)[0])
            collection.insert_one({'filename': os.path.basename(chunck), 'nb_lines_in_file': nb_lines_in_file-1, 'nb_inserted':nb_inserted})
        except subprocess.CalledProcessError:
            print("Erreur proces mongoimport")
            pass
    exit()
