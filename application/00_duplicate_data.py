import csv
import random
import string


def random_string_generator(str_size, allowed_chars):
    return ''.join(random.choice(allowed_chars) for x in range(str_size))


chars = string.ascii_letters + string.digits
for num_file in range(20100310, 20100320):
    with open("../input_data/input_2k.csv", "r") as fhr:
        with open("../input_data/%s_input.csv" % str(num_file), "w") as fhw:
            csv_reader = csv.reader(fhr, delimiter=',')
            csv_writer = csv.writer(fhw, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    print(f'Column names are {", ".join(row)}')
                    line_count += 1
                else:
                    key = row[0]
                    v = row[1]
                    csv_writer.writerow([key, v])
                    key_len = len(key)
                    for i in range(0, 10):
                        csv_writer.writerow([random_string_generator(key_len, chars), v])

                    line_count += 1
            print(f'Processed {line_count} lines.')

