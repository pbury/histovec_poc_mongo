import configparser


class Common:

    def __init__(self):
        self.bootstrap_servers = "broker:9092"
        self.schema_registry_url = "http://schema-registry:8081"
        self.config = configparser.ConfigParser()
        self.config.read('histovec_kafka.ini')

    @staticmethod
    def get_mongo():
        mongo_host = "mongo_histovec"
        mongo_port = 27017
        return mongo_host, mongo_port
