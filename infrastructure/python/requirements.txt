pymongo==3.10.1
kafka==1.3.5
python-dotenv==0.14.0
avro==1.10.2
confluent-kafka==1.6.1
requests==2.24.0